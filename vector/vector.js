/* Le JS placé ici n’affectera que les utilisateurs de l’habillage Vector */
/**** function displayTimer.js
 * Original by Patrick Westerhoff [poke]
 */
function displayTimer ()
{
  if ( typeof( timerDisplay ) !== 'undefined' && timerDisplay === false )
    return;

  var date;
  var timerParent = document.getElementById( 'p-personal' ).getElementsByTagName( 'ul' )[0];
  var timerLink   = document.createElement( 'a' );
  var timerObj    = document.createElement( 'li' );
  timerLink.href               = '/wiki/' + mw.config.get('wgPageName') + '?action=purge';
  timerLink.title              = 'Purge le cache de la page et réactualise son contenu.'
  timerObj.id                  = 'pt-timer';
  timerObj.style.textTransform = 'none';
  timerObj.style.fontWeight    = 'bold';
  timerObj.appendChild( timerLink );
  timerParent.insertBefore( timerObj, timerParent.firstChild );

  function actualizeUTC ()
  {
  timerDate             = new Date();
  timezone              = timerDate.getTimezoneOffset()

  timediff              = - timezone / 60

  timeHours = timerDate.getUTCHours() + timediff

  if (timeHours < 24) {
     hours = timeHours
  } else {
     hours = timeHours - 24
  }

  timerLink.innerHTML   =  (hours < 10 ? '0' : '') + (hours) + ':'
                          + (timerDate.getMinutes() < 10 ? '0' : '') + timerDate.getUTCMinutes() + ':'
                          + ( timerDate.getSeconds() < 10 ? '0' : '' ) + timerDate.getUTCSeconds() + ' (local)';
  }

  function actualizeCustom ()
  {
    timerDate           = new Date();
    timerDate.setMinutes( timerDate.getMinutes() + timerDate.getTimezoneOffset() + timerTimezone * 60 );
    timerLink.innerHTML = ( timerDate.getHours()   < 10 ? '0' : '' ) + timerDate.getHours()   + ':'
                        + ( timerDate.getMinutes() < 10 ? '0' : '' ) + timerDate.getMinutes() + ':'
                        + ( timerDate.getSeconds() < 10 ? '0' : '' ) + timerDate.getSeconds()
                        + ' (UTC' + ( timerTimezone < 0 ? '' : '+' ) + timerTimezone + ')';
  }

  // start
  if ( typeof( timerTimezone ) !== 'number' )
  {
    actualizeUTC();
    setInterval( actualizeUTC, 1000 );
  }
  else
  {
    actualizeCustom();
    setInterval( actualizeCustom, 1000 );
  }
}
$(displayTimer);


function setFooter() {
  var top = 0;
  var width = 0;

  top = $('#mw-panel').outerHeight() + 160;

  if ($('#content').outerHeight() + 80 < top) {
    top -= $('#content').outerHeight() + 80;
  } else {
    top = 0;
  }

  var fixBottom = $(window).outerHeight() - (80 + 25 + $('#content').outerHeight() + top + $('#footer').outerHeight());
  if (fixBottom > 0) {
    top += fixBottom;
  }

  $('#footer').css({
    position : 'relative',
    top : top + 'px',
    zIndex : -1
  });

  width = $('body').width() - 196 - 20;

  $('#footer').css('width', width + 'px');
}

$(window).on("load",setFooter);
$(window).resize(setFooter);
$('#mw-panel .portal:not(.persistent) > h3').mousedown(function () {setTimeout(setFooter, 200)})

/* Vekktor, a Vector fix for tiny screens and tiny genious, by Skrool*/
// Remove the setFooter event listener
$(window).off("load",setFooter);
$(window).off("resize", setFooter);

// Display the panel
function toggle_portal_nav(e){
    var p = document.getElementById("mw-panel");

    if (p.classList.contains("force-open")) {
        p.classList.remove("force-open");
        e.target.innerText = "▶";
    }
    else {
        p.classList.add("force-open");
        e.target.innerText = "◀";
    }
}

// Display the panel toggle button
function display_portal_toggle_button(){
    var button = document.createElement("button");
    button.innerText = "▶";
    button.title = "Afficher le menu latéral";
    button.onclick = toggle_portal_nav;
    button.classList.add("portal_toggle_button");
    document.getElementById('mw-panel').appendChild(button);
}
$(display_portal_toggle_button);
