/* Tout JavaScript ici sera chargé avec chaque page accédée par n’importe quel utilisateur. */
/*<nowiki>*/
/** additional scripts **/
if (
  mw.config.get("wgIsArticle") ||
  window.location.href.indexOf("action=submit") > -1 ||
  mw.config.get("wgNamespaceNumber") == -1
) {
  addScript("MediaWiki:Géolocalisation.js‎");
  addScript("MediaWiki:CollapsibleTables.js");
  addScript("MediaWiki:InitialSort.js");
  addScript("MediaWiki:Masthead.js");
  addScript("MediaWiki:Recherche_de_code_en_jeu.js");
  //addScript( 'MediaWiki:SnowStorm.js');
  //addScript( 'MediaWiki:CollapsibleSections.js' );
  //addScript( 'MediaWiki:Jquery.cluetip.js' );
  //addScript( 'Utilisateur:Till034/itemtooltip.js' );

  $(window).on("load", function () {
    new Géolocalisation();
    /*new CollapsibleTables();
    //new CollapsibleSections();
    new InitialSort();
    //new Itemtooltip();
    new TSPreview();
    diffwithFix();

    // Extension for the deletion drop down list
    if ( mw.config.get('wgAction') == 'delete' && ( delReasonBtn = document.getElementById( 'wpConfirmB' ) ) )
    {
      var delReasonList        = document.getElementById( 'wpDeleteReasonList' );
      var delLink              = document.createElement( 'a' );
      delLink.href             = 'javascript:void(0);'
      delLink.title            = document.getElementById( 'wpReason' ).value;
      delLink.style.fontSize   = '0.9em';
      delLink.style.marginLeft = '1em';
      delLink.onclick          = function()
      {
        document.getElementById( 'wpReason' ).value = this.title;
      }
      delReasonList.onchange   = function ()
      {
        document.getElementById( 'wpReason' ).value = '';
        this.onchange = null;
      }
      delLink.appendChild( document.createTextNode( 'restore default reason' ) );
      delReasonBtn.parentNode.appendChild( delLink );
      delete delLink, delReasonList, delReasonBtn;
    }*/
  });
}

/**** function addScript.js
 * by Patrick Westerhoff [poke]
 */
function addScript(pagename) {
  var script = document.createElement("script");
  pagename = encodeURI(pagename.replace(" ", "_"));
  script.src =
    "/index.php?title=" + pagename + "&action=raw&ctype=text/javascript";
  script.type = "text/javascript";

  document.getElementsByTagName("head")[0].appendChild(script);
}

/**** function diffwithFix.js
 * by Patrick Westerhoff [poke]
 */
function diffwithFix() {
  var diffSpan = document.getElementById("diffwith");
  if (diffSpan == undefined) return;

  var diffLink = diffSpan.getElementsByTagName("a")[0];
  var diffTitle = diffSpan.title;
  var xmlHttp;

  try {
    xmlHttp = new XMLHttpRequest();
  } catch (e) {
    try {
      xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {
        diffSpan.style.fontSize = "90%";
        diffSpan.innerHTML = "(Automated diff <b>not available</b>.)";
        return;
      }
    }
  }

  xmlHttp.onreadystatechange = function () {
    if (xmlHttp.readyState != 4) return;

    revs = xmlHttp.responseXML.getElementsByTagName("rev");

    if (revs.length > 0) {
      diffLink.href += "&oldid=" + revs[0].getAttribute("revid");
      diffSpan.title = "";
    }
  };
  xmlHttp.open(
    "GET",
    "/api.php?format=xml&action=query&prop=revisions&rvprop=ids&rvlimit=1&titles=" +
      diffTitle,
    true
  );
  xmlHttp.send(null);
}

/**** Toolbar jquery add button */
(function (mw, $) {
  "use strict";
  var addExtraButtons = function () {
    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/b/ba/Button_paragraphe_3.png",
      speedTip: "Sous-titre de niveau 3",
      tagOpen: "\n=== ",
      tagClose: " ===",
      sampleText: "Sous-titre",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/6/61/Button_paragraphe_4.png",
      speedTip: "Sous-titre de niveau 4",
      tagOpen: "\n==== ",
      tagClose: " ====",
      sampleText: "Sous-titre ",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/7/79/Button_paragraphe_5.png",
      speedTip: "Titre de niveau 5",
      tagOpen: "\n===== ",
      tagClose: " =====",
      sampleText: "Sous-titre",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/en/c/c8/Button_redirect.png",
      speedTip: "Créer une redirection",
      tagOpen: "#REDIRECTION [[",
      tagClose: "]]",
      sampleText: "Page",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/5/58/Button_small.png",
      speedTip: "Ecrire un texte en petit",
      tagOpen: "<small>",
      tagClose: "</small>",
      sampleText: "Texte",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://wiki-fr.guildwars2.com/images/d/dc/Button_Template.png",
      speedTip: "Ajouter un modèle",
      tagOpen: "{{",
      tagClose: "}}",
      sampleText: "Nom du modèle",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/a/a4/TableStart.png",
      speedTip: "Ajouter un tableau",
      tagOpen: "{| {{STDT}} \n!",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/7/71/TableCell.png",
      speedTip: "Ajouter une case",
      tagOpen: "\n|",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/4/4c/TableRow.png",
      speedTip: "Ajouter une ligne",
      tagOpen: "\n|-",
    });

    mw.toolbar.addButton({
      imageFile:
        "https://upload.wikimedia.org/wikipedia/commons/0/06/TableEnd.png",
      speedTip: "Terminer le tableau",
      tagOpen: "\n|}",
    });
  };

  if ($.inArray(mw.config.get("wgAction"), ["edit", "submit"]) !== -1) {
    mw.loader.using("user.options", function () {
      if (!mw.user.options.get("usebetatoolbar")) {
        mw.loader.using("mediawiki.action.edit", function () {
          $(addExtraButtons);
        });
      }
    });
  }
})(mediaWiki, jQuery);

/**
 * Force users to select a license option when uploading files (Wiki EN)
 *   also allows manual entry in Summary for 'ArenaNet image' and 'User image' licenses
 */
$("#mw-upload-form").submit(function (event) {
  var licenseValue = $("#wpLicense").val();
  var uploadDescription = $("#wpUploadDescription").val();
  if (
    licenseValue == "" &&
    !uploadDescription.match(/(Licensing|ArenaNet image|User image)/i)
  ) {
    alert("Merci de choisir une licence dans la liste déroulante.");
    event.preventDefault();
  }
});

/**** Code pour infobox **/
jQuery(document).ready(function setentêteicône() {
  var top = 20;

  if (
    $(".entêteicône").outerHeight() > 25 &&
    $(".entêteicône").outerHeight() < 50
  ) {
    top -= 5;
  } else if ($(".entêteicône").outerHeight() > 51) {
    top -= 17;
  } else {
    top += 0;
  }
  $(".entêteicône").css({ "margin-top": top + "px", "margin-bottom": "26px" });
});

//correction des espaces blancs entre les listes à plusieurs colonnes
RLQ.push(function () {
  jQuery(document).ready(
    function formatageDesListesEnColonnesGenereesParUneRequete() {
      var list = document.getElementsByClassName("column request");
      for (var i = 0; i < list.length; i++) {
        var divlist = list[i].getElementsByTagName("DIV");
        for (var j = 0; j < divlist.length; j++) {
          if (typeof divlist[j].style !== "undefined") {
            if (divlist[j].style.cssFloat === "left") {
              divlist[j].style.cssFloat = "none";
              divlist[j].style.display = "inline-block";
              divlist[j].style.verticalAlign = "top";
            }
          }
        }
        var plist = list[i].getElementsByTagName("P");
        for (var j = plist.length - 1; j >= 0; j--) {
          if (
            plist[j].children.length === 1 &&
            plist[j].children[0].tagName === "BR"
          ) {
            plist[j].remove();
          } else {
            var brlist = plist[j].getElementsByTagName("BR");
            for (var k = brlist.length - 1; k >= 0; k--) {
              if (typeof brlist[k].style !== "undefined") {
                if (brlist[k].style.clear === "both") {
                  brlist[k].remove();
                }
              }
            }
          }
        }
      }
    }
  );
});

//Ajout de la section 'Nous suivre' dans la sidebar
function ModifySidebar(action, section, name, link, icon, iconsize) {
  try {
    switch (section) {
      case "languages":
        var target = "p-lang";
        break;
      case "toolbox":
        var target = "p-tb";
        break;
      case "navigation":
        var target = "p-navigation";
        break;
      default:
        var target = "p-" + section.replace(/ /g, "_");
        break;
    }

    if (action == "add") {
      var node = document
        .getElementById(target)
        .getElementsByTagName("div")[0]
        .getElementsByTagName("ul")[0];

      var aNode = document.createElement("a");
      var liNode = document.createElement("li");

      if (typeof icon != "undefined" && icon != null) {
        var imgNode = document.createElement("img");
        imgNode.setAttribute("src", icon);
        imgNode.style.maxHeight = iconsize;
        imgNode.style.maxWidth = iconsize;
        aNode.appendChild(imgNode);
      }

      if (name != null) {
        aNode.appendChild(document.createTextNode("\u00A0"));
        aNode.appendChild(document.createTextNode(name));
      }
      aNode.setAttribute("href", link);
      liNode.appendChild(aNode);
      liNode.className = "plainlinks";
      node.appendChild(liNode);
    }

    if (action == "remove") {
      var list = document
        .getElementById(target)
        .getElementsByTagName("div")[0]
        .getElementsByTagName("ul")[0];

      var listelements = list.getElementsByTagName("li");

      for (var i = 0; i < listelements.length; i++) {
        if (
          listelements[i].getElementsByTagName("a")[0].innerHTML == name ||
          listelements[i].getElementsByTagName("a")[0].href == link
        ) {
          list.removeChild(listelements[i]);
        }
      }
    }
  } catch (e) {
    // let's just ignore what's happended
    return;
  }
}

function CustomizeModificationsOfSidebar() {
  // adds discord link to Nous suivre
  ModifySidebar(
    "add",
    "Nous suivre",
    "Discord",
    "https://discord.gg/6pSGs89",
    "https://wiki-fr.guildwars2.com/images/1/17/Discord-icon.svg",
    "18px"
  );
  // adds Twitter link to Nous suivre
  ModifySidebar(
    "add",
    "Nous suivre",
    "Twitter",
    "https://twitter.com/GW2WFR",
    "https://wiki-fr.guildwars2.com/images/4/4f/Twitter-logo.svg",
    "18px"
  );
  // adds Youtube link to Nous suivre
  ModifySidebar(
    "add",
    "Nous suivre",
    "YouTube",
    "https://www.youtube.com/channel/UCXdAWL9yiAhXPLuhPb-Y_wQ",
    "https://wiki-fr.guildwars2.com/images/3/32/Yt_icon_rgb.svg",
    "18px"
  );
}

jQuery(CustomizeModificationsOfSidebar);

/*</nowiki>*/
