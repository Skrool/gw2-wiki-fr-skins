/*** HALLOWEEN ***/
function randomBackgroundHalloween() {
  var baseUrl = "/images";
  var array = [
    /* [Url header, Url logo, link tab colors ] */
    [
      "/7/7d/Halloween_-_Clocher_-_header.png",
      "/b/b4/Logo_-_Halloween_-_Clocher.png",
      "#EDE1F2",
    ],
    [
      "/e/ef/Halloween_-_Arche_du_Lion_-_header.png",
      "/3/3e/Logo_-_Halloween_-_vert.png",
      "#BECFA5",
    ],
    [
      "/3/3b/Halloween_-_Lune_-_header.png",
      "/c/c3/Halloween_-_Lune_-_logo.png",
      "#FFF5A7",
    ],
    [
      "/0/0c/Halloween_-_Roi_Dement_-_header.png",
      "/4/40/Halloween_-_Roi_Dement_-_logo.png",
      "#FAB950",
    ],
  ];

  var randomIndex = Math.floor(Math.random() * array.length);

  $("body").css(
    "--header-image",
    "url('" + baseUrl + array[randomIndex][0] + "')"
  );
  $("#p-logo a").css(
    "--logo-image",
    "url('" + baseUrl + array[randomIndex][1] + "')"
  );

  function randomColor() {
    if ($("#upperBar ul li a").length === 0) {
      setTimeout(randomColor, 10);
    } else {
      $("#upperBar ul li a").css("color", array[randomIndex][2]);
    }
  }
  randomColor();
}
$(randomBackgroundHalloween);

function randomMainPageHalloween() {
  var baseUrl = "/images";
  var array = [
    "/3/3a/Halloween_-_accueil_-_roi_et_bourreau.png",
    "/b/b4/Halloween_-_accueil_-_sylvari_et_humaine.png",
  ];

  var randomIndex = Math.floor(Math.random() * array.length);

  $("#accueil").css({
    "background-image": "url('" + baseUrl + array[randomIndex] + "')",
    "background-repeat": "no-repeat",
    "background-position": "center",
  });
}
$(randomMainPageHalloween);
