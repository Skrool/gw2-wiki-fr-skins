/** CONFETTI (ﾉ^ヮ^)ﾉ :・ﾟ✧ * */
function NeigeraOuNeigeraPas() {
var para = document.createElement("li");
para.id = "neige";

var box = document.createElement("input");
box.type = "checkbox";
box.id = "neigeOuPas";
box.title = "Activer ou désactiver les confettis.";
box.checked = JSON.parse(localStorage.getItem("snowToggle"));

var label = document.createElement("label");
label.title = "Activer ou désactiver les confettis.";
label.innerHTML = "<span>Confettis :</span>";

var element = document.getElementById("p-personal");
var child = element.getElementsByTagName("ul")[0];

label.appendChild(box);
para.appendChild(label);
child.insertBefore(para, child.firstChild);

$("#neigeOuPas").click(function () {
        if (typeof snowStorm === "undefined" || snowStorm === null) {
            console.warn("NeigeraOuNeigeraPas : snowStorm not loaded.");
            return;
        }

        if (box.checked) {
            snowStorm.show();
            snowStorm.resume();
        } else {
            snowStorm.stop();
            snowStorm.freeze();
        }

        localStorage.removeItem("snowToggle");
        localStorage.setItem("snowToggle", JSON.stringify(box.checked));
    });
}

function initNeige() {
    if (typeof snowStorm === "undefined" || snowStorm === null) {
        console.warn("initNeige : snowStorm not loaded");
        return;
}

var checked = JSON.parse(localStorage.getItem("snowToggle"));
    if (!checked) {
        snowStorm.start();
        snowStorm.stop();
        snowStorm.freeze();
    } else {
        snowStorm.start();
    }
}

$(document).ready(function () {
    var pagename = "MediaWiki:SnowStorm.js";
    pagename = encodeURI(pagename.replace(" ", "_"));
    var src =
        "/index.php?title=" + pagename + "&action=raw&ctype=text/javascript";
    if (typeof snowStorm === "undefined")
        $.getScript(src, function () {
            // Wrapper on top of the body to contain all the confetti (and apply styling)
            var wrapper = document.createElement("div");
            wrapper.id = "confetti-wrapper"
            document.body.appendChild(wrapper);

            // Additional settings
            window.snowStorm.animationInterval = 15;
            window.snowStorm.flakeWidth = 8;
            window.snowStorm.flakeHeight = 12;
            window.snowStorm.vMaxX = 1;
            window.snowStorm.usePositionFixed = false;
            window.snowStorm.targetElement = wrapper;

            NeigeraOuNeigeraPas();
            initNeige();
        });
});
