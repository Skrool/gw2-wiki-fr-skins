# Purpose
This git repository is about wiki appearance for the gw2 French wiki.

# Structure
* Common: common.css and common.js
* Dark: Css and Js for base of dark theme
* Four winds: Css for Four winds Festival
* Halloween: Css and Js for Halloween
* Lunar_new_year: Css and Js for lunar new year festival
* Minerva: Base Css and Js for MinevervaNeue appearance
* SAB: Css for Super Adventure Box
* Vector: Base Css and Js for Vector appearance
* Wintersday: Css and Js for Wintersday