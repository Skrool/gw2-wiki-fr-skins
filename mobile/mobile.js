/* Le Javascript placé ici n’affectera que les utilisateurs du site mobile */
/* Ajout de liens dans le menu Minerva
 * Base écrite par EN:User:Poke
 */
RLQ.push(function updateMobileMenu() {
  var group1 = [
    {
      name: "home",
      components: [
        {
          text: "Accueil",
          href: "/wiki/Accueil",
          class: "mw-ui-icon mw-ui-icon-before mw-ui-icon-minerva-home ",
          "data-event-name": "home",
        },
      ],
    },
    {
      name: "recent-changes",
      components: [
        {
          text: "Modifications récentes",
          href: "/wiki/Special:RecentChanges",
          class: "mw-ui-icon mw-ui-icon-before mw-ui-icon-minerva-rc ",
          "data-event-name": "recent-changes",
        },
      ],
    },
    {
      name: "random",
      components: [
        {
          text: "Page au hasard",
          href: "/wiki/Special:Random#/random",
          class: "mw-ui-icon mw-ui-icon-before mw-ui-icon-minerva-random ",
          id: "randomButton",
          "data-event-name": "random",
        },
      ],
    },
  ];
  var group2 = [
    {
      name: "ask",
      components: [
        {
          text: "Poser une question",
          href: "/wiki/Aide:Poser_une_question",
          class: "mw-ui-icon mw-ui-icon-before mw-ui-icon-minerva-aq ",
        },
      ],
    },
    {
      name: "admin-noticeboard",
      components: [
        {
          text: "Avertir un administrateur",
          href: "/wiki/GW2Wiki:Avertir_un_administrateur",
          class: "mw-ui-icon mw-ui-icon-before mw-ui-icon-minerva-an ",
        },
      ],
    },
    {
      name: "report-bugs",
      components: [
        {
          text: "Signaler un bug",
          href: "/wiki/GW2Wiki:Signaler_un_bug_du_wiki",
          class: "mw-ui-icon mw-ui-icon-before mw-ui-icon-minerva-rb ",
        },
      ],
    },
  ];

  wgMinervaMenuData.groups.splice(0, 1, group1, group2);
});
