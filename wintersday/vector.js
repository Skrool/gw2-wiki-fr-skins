/*** HIVERNEL ***/
function randomBackgroundWintersday() {
  var baseUrl = "/images";
  var array = [
    /* [Url header, Url logo, link tab colors ] */
    //["/3/34/Header_-_Hivernel_-_Logo.png", "", "#dbe6fd"],
    ["/9/98/Header_-_Hivernel_-_Promontoire_Divin_-_Logo.png", "", "#e9eef4"],
    ["/6/6f/Header_-_Hivernel_-_raid.png", "", "#bcd2ed"],
    ["/a/a8/Header_-_Hivernel_-_boule_de_neige_-_Logo.png", "", "#ee123b"],
    ["/1/11/Header_-_Hivernel_2020.png", "/a/ac/Logo_-_Hivernel_2020.png", "var(--external-link)"]
    //["/b/ba/Header_-_Hivernel_-_Ho-Ho-Tron_-_Logo.png", "", "#f7d01b"],
  ];

  var randomIndex = Math.floor(Math.random() * array.length);

  $("body").css(
    "--header-image",
    "url('" + baseUrl + array[randomIndex][0] + "')"
  );
  $("#p-logo a").css(
    "--logo-image",
    "url('" + baseUrl + array[randomIndex][1] + "')"
  );

  function randomColor() {
    if ($("#upperBar ul li a").length === 0) {
      setTimeout(randomColor, 10);
    } else {
      $("#upperBar ul li a").css("color", array[randomIndex][2]);
    }
  }
  randomColor();
}
$(randomBackgroundWintersday);

function randomMainPageWintersday() {
  var baseUrl = "/images";
  var array = [
    /* [Url left, Url right, size left, size right ] */
    [
      "/7/7d/Hivernel-Htg17-left.png",
      "/7/75/Hivernel-Htg17-right.png",
      "72%",
      "82%",
    ],
    [
      "/9/9b/Hivernel-Idaida-left.png",
      "/3/3c/Hivernel-Idaida-right.png",
      "75%",
      "64%",
    ],
  ];

  var randomIndex = Math.floor(Math.random() * array.length);

  $("#mainpage_left").css({
    "background-image": "url('" + baseUrl + array[randomIndex][0] + "')",
    "background-repeat": "no-repeat",
    position: "absolute",
    "background-position-x": "left",
    top: "150px",
    "background-size": array[randomIndex][2],
  });

  $("#mainpage_right").css({
    "background-image": "url('" + baseUrl + array[randomIndex][1] + "')",
    "background-repeat": "no-repeat",
    position: "absolute",
    "background-position-x": "right",
    top: "600px",
    "background-size": array[randomIndex][3],
  });
}
$(randomMainPageWintersday);

/** SNOW (ﾉ^ヮ^)ﾉ :・ﾟ✧ * */
function NeigeraOuNeigeraPas() {
  var para = document.createElement("li");
  para.id = "neige";

  var box = document.createElement("input");
  box.type = "checkbox";
  box.id = "neigeOuPas";
  box.title = "Activer ou désactiver la neige.";
  box.checked = JSON.parse(localStorage.getItem("snowToggle"));

  var label = document.createElement("label");
  label.title = "Activer ou désactiver la neige.";
  label.innerHTML = "<span>Neige :</span>";

  var element = document.getElementById("p-personal");
  var child = element.getElementsByTagName("ul")[0];

  label.appendChild(box);
  para.appendChild(label);
  child.insertBefore(para, child.firstChild);

  $("#neigeOuPas").click(function () {
    if (typeof snowStorm === "undefined" || snowStorm === null) {
      console.warn("NeigeraOuNeigeraPas : snowStorm not loaded.");
      return;
    }

    if (box.checked) {
      snowStorm.show();
      snowStorm.resume();
    } else {
      snowStorm.stop();
      snowStorm.freeze();
    }

    localStorage.removeItem("snowToggle");
    localStorage.setItem("snowToggle", JSON.stringify(box.checked));
  });
}

function initNeige() {
  if (typeof snowStorm === "undefined" || snowStorm === null) {
    console.warn("initNeige : snowStorm not loaded");
    return;
  }

  var checked = JSON.parse(localStorage.getItem("snowToggle"));
  if (!checked) {
    snowStorm.start();
    snowStorm.stop();
    snowStorm.freeze();
  } else {
    snowStorm.start();
  }
}

$(document).ready(function () {
  var pagename = "MediaWiki:SnowStorm.js";
  pagename = encodeURI(pagename.replace(" ", "_"));
  var src =
    "/index.php?title=" + pagename + "&action=raw&ctype=text/javascript";
  //if (typeof snowStorm == 'undefined') $.loadScript(src, function(){
  if (typeof snowStorm == "undefined")
    $.getScript(src, function () {
      NeigeraOuNeigeraPas();
      initNeige();
    });
});
