/* Function to parse a string representing a color to an object */
function parseColor(str) {
  let color = ((str) => {
    let m;
    m = str.match(/^#([0-9a-f]{3})$/i);
    if (m) {
      // #xxx
      return {
        r: parseInt(m[1].charAt(0), 16) * 0x11,
        g: parseInt(m[1].charAt(1), 16) * 0x11,
        b: parseInt(m[1].charAt(2), 16) * 0x11,
        a: 255,
      };
    }

    m = str.match(/^#([0-9a-f]{6})$/i);
    if (m) {
      // #xxxxxx
      return {
        r: parseInt(m[1].substr(0, 2), 16),
        g: parseInt(m[1].substr(2, 2), 16),
        b: parseInt(m[1].substr(4, 2), 16),
        a: 255,
      };
    }

    m = str.match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i);
    if (m) {
      // rgb(x, x, x)
      return {
        r: parseInt(m[1]),
        g: parseInt(m[2]),
        b: parseInt(m[3]),
        a: 255,
      };
    }

    m = str.match(
      /^rgba\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i
    );
    if (m) {
      // rgba(x, x, x)
      return {
        r: parseInt(m[1]),
        g: parseInt(m[2]),
        b: parseInt(m[3]),
        a: parseInt(m[4]),
      };
    }
    return { r: 0, g: 0, b: 0, a: 0 };
  })(str);
  color.brightness = Math.sqrt(
    0.241 * color.r * color.r +
      0.691 * color.g * color.g +
      0.068 * color.b * color.b
  );

  return color;
}

/*
 * Function that force the text to be in black if the background is too light
 */
function correctContrast(startingPoint) {
  function pass(element, bgColor, textColor) {
    let style = window.getComputedStyle(element, null);

    if (style.color) {
      let tmp = parseColor(style.color);
      textColor = tmp.a > 0 ? tmp : textColor;
    }
    if (style.backgroundColor) {
      let tmp = parseColor(style.backgroundColor);
      bgColor = tmp.a > 0 ? tmp : bgColor;
    }

    if (textColor && bgColor) {
      if (textColor.brightness > 150 && bgColor.brightness > 150) {
        element.classList.add("contrastDark");
        textColor = parseColor("#000");
        element.setAttribute("changed", "yes");
      } else if (textColor.brightness < 130 && bgColor.brightness < 130) {
        element.classList.add("contrastLight");
        textColor = parseColor("#fff");
        element.setAttribute("changed", "yes");
      }
    }

    for (let i = 0; i < element.children.length; i++) {
      pass(element.children[i], bgColor, textColor);
    }
  }
  pass(startingPoint ? startingPoint : document.body, null, null);
}
